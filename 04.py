def test_alternate():
    assert alternate("beabeefeab") == 5
    assert alternate("aab") == 2
    assert alternate("aabbccdd") == 8
    assert alternate("abcdefghijklmnopqrstuvwxyz") == 26
    assert alternate("aaaaaa") == 3

if __name__ == "__main__":
    test_alternate()
