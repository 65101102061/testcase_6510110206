import unittest

class TestFunnyString(unittest.TestCase):
    def test_funnyString(self):
        # Test 1: Given test case
        s = "acxz"
        expected = "Funny"
        result = funnyString(s)
        self.assertEqual(result, expected)

        # Test 2: Not funny string
        s = "bcxz"
        expected = "Not Funny"
        result = funnyString(s)
        self.assertEqual(result, expected)
        
        # Test 3: Empty string
        s = ""
        expected = "Funny"
        result = funnyString(s)
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()
