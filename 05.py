import unittest

class TestGridChallenge(unittest.TestCase):
    def test_grid_challenge(self):
        grid = ['ebacd', 'fghij', 'olmkn', 'trpqs', 'xywuv']
        self.assertEqual(gridChallenge(grid), "YES")

        grid = ['abcde', 'fghij', 'klmno', 'pqrst', 'uvwxy']
        self.assertEqual(gridChallenge(grid), "YES")

        grid = ['abcde', 'fghik', 'lmnoj', 'pqrst', 'uvwxy']
        self.assertEqual(gridChallenge(grid), "NO")

if __name__ == '__main__':
    unittest.main()
