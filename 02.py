import unittest

class TestAlternatingCharacters(unittest.TestCase):
    def test_alternatingCharacters(self):
        # Test 1: Given test case
        s = "AAAA"
        expected = 3
        result = alternatingCharacters(s)
        self.assertEqual(result, expected)

        # Test 2: All unique characters
        s = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        expected = 0
        result = alternatingCharacters(s)
        self.assertEqual(result, expected)
        
        # Test 3: Repeated characters
        s = "AAAAAAAAAAAAA"
        expected = 12
        result = alternatingCharacters(s)
        self.assertEqual(result, expected)

if __name__ == '__main__':
    unittest.main()

